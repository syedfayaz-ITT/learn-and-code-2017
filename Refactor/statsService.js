/*global angular, Konva*/
/*jslint nomen:true*/
(function () {
  'use strict';

  /**
   * StatsService Responsible to calculate Stats based on assets and levels we have in UI
   * @param Configuration, translateFilter, LevelType, AssetType, StageHelper, Utils, AssetService, LevelService,
   LevelEntityMapService
   * @returns Service Object.
   * @constructor
   */
  function StatsService(Configuration, translateFilter, LevelType, AssetType, StageHelper, Utils, AssetService, LevelService,
                        LevelEntityMapService) {

    var ASSET = 'asset',
        assetStats, levelStats;


    /**
     * Responsible to create assets for SquesncialLevels like Country Site and building only
     * Floor and project we need to get assets for subLevels only beucase floor will not have subLevels and for project
     * we need to display all assets Plotted and unplotted only
     * @param plottedAssets
     * @param selectedLevel
     * @returns {Array|*}
     */
    function getAssetsForSequentialLevels(plottedAssets, selectedLevel) {
      var floors, entityMap, entityMaps = [], currentLevelPlottedAssets = [],
          subLevels = LevelService.getSubLevels(selectedLevel._id);

      // retrive floors from subLevels
      floors = subLevels.filter(function (level) {
        return level.type === LevelType.FLOOR;
      });

      // Get level Entitymap for each floor
      floors.forEach(function (floor, index) {
        entityMap = LevelEntityMapService.getLevelEntityMaps(floor._id);
        entityMaps = entityMaps.concat(entityMap);
      });

      entityMaps.forEach(function (entityMap) {
        if (entityMap.entityType === ASSET) {
          plottedAssets.forEach(function (asset) {
            if (entityMap.entityId === asset._id) {
              currentLevelPlottedAssets.push(asset);
            }
          })
        }
      });
      return currentLevelPlottedAssets;
    }


    /**
     * Get asset stats
     * @param assets Need to pass
     * @param selectedLevel Need to pass
     * @returns stats
     */
    function getAssetStats(assets, selectedLevel) {
      var plottedAssets;
      assetStats = {
        nonInventory: 0,
        inventory: 0,
        unplotted: 0,
        plotted: 0,
        singleFunctionMono: 0,
        singleFunctionColor: 0,
        multiFunctionMono: 0,
        multiFunctionColor: 0,
        unknownDevice: 0,
        others: 0,
        oldAssets: 0,
        oldAssets10Years: 0,
        uniqueModel: 0,
        uniqueMake: 0,
        uniqueManufacturer: {},
        uniqueTonerCartridge: 0,
        inventoryAssets: [],
        models: [],
        pageCountMono: 0,
        pageCountColor: 0,
        pageCountTotal: 0,
        scanCount: 0,
        copyCount: 0,
        faxReceiveCount: 0
      };

      plottedAssets = assets.filter(function (asset) {
        return asset.plotted;
      });

      // Check current selected Level for SequentialLevels i.e site building & country we need to get assets for sublevel also
      // if selected level is floor then we need to show only current floor plotted asses
      if (selectedLevel && selectedLevel.type !== LevelType.PROJECT && selectedLevel.type !== LevelType.FLOOR) {
        assets = getAssetsForSequentialLevels(plottedAssets, selectedLevel);
      }
      else if (selectedLevel && selectedLevel.type === LevelType.FLOOR) {
        var lem, currentPlottedAssets = [];
        lem = LevelEntityMapService.getLevelEntityMapsByEntities(selectedLevel._id, ASSET);
        lem.forEach(function (map) {
          var asset = plottedAssets.find(function (asset) {
            return map.entityId === asset._id;
          });
          currentPlottedAssets.push(asset);
        });
        assets = currentPlottedAssets;
      }

      if (assets.length) {
        assetStats.inventoryAssets = assets.filter(function (asset) {
          if(asset && asset.inventory){
            return asset.inventory;
          }

        });
        assetStats.inventory = assetStats.inventoryAssets.length;
        assetStats.nonInventory = assets.length - assetStats.inventory;
        for (var index = 0; index < assetStats.inventoryAssets.length; index++) {
          if (assetStats.inventoryAssets[index].plotted) {
            assetStats.plotted++;
          }
          else {
            assetStats.unplotted++;
          }

          // Get Asset age
          if (assetStats.inventoryAssets[index].dateIntroduced !== undefined) {
            var age = Utils.getAge(assetStats.inventoryAssets[index].dateIntroduced);
            if (age > Configuration.ASSET_AGE.FIVE && age < Configuration.ASSET_AGE.TEN) {
              assetStats.oldAssets++;
            }
            else if (age > Configuration.ASSET_AGE.TEN) {
              assetStats.oldAssets10Years++;
            }
          }
          switch (assetStats.inventoryAssets[index].deviceType) {
            case AssetType.MULTIFUNCTION_MONO:
              assetStats.multiFunctionMono++;
              break;
            case AssetType.MULTIFUNCTION_COLOR:
              assetStats.multiFunctionColor++;
              break;
            case AssetType.PRINT_MONO:
              assetStats.singleFunctionMono++;
              break;
            case AssetType.PRINT_COLOR:
              assetStats.singleFunctionColor++;
              break;
            case AssetType.UNKNOWN:
              assetStats.unknownDevice++;
              break;
            default:
              assetStats.others++;
              break;
          }
          // Check if get request for selected level then only we need to get counts of reading for only plotted Assets
          if (selectedLevel && assetStats.inventoryAssets[index].plotted) {
            assetStats.pageCountMono += parseInt(assetStats.inventoryAssets[index].monoTotalMonthly) || 0;
            assetStats.pageCountColor += parseInt(assetStats.inventoryAssets[index].colorTotalMonthly) || 0;
            assetStats.scanCount += parseInt(assetStats.inventoryAssets[index].scanTotalMonthly) || 0;
            assetStats.copyCount += parseInt(assetStats.inventoryAssets[index].copyTotalMonthly) || 0;
            assetStats.faxReceiveCount += parseInt(assetStats.inventoryAssets[index].faxReceivedMonthly) || 0;
          }
        }

        assetStats.models = assetStats.inventoryAssets.map(function (asset) {
          return asset.model;
        });

        //remove duplicate entries based on models
        assetStats.models = assetStats.models.filter(function (value, index) {
          return value && assetStats.models.indexOf(value) === index;
        });

        //remove duplicate entries based on Make
        var manufacturers = assetStats.inventoryAssets.map(function (asset) {
          return asset.make ? asset.make : '';
        });

        // get make count
        manufacturers.map(function (manufacturer) {
          if (manufacturer in assetStats.uniqueManufacturer) {
            assetStats.uniqueManufacturer[manufacturer]++;
          }
          else {
            assetStats.uniqueManufacturer[manufacturer] = 1;
          }
        });


        for (var key in assetStats.uniqueManufacturer) {
          if (key === '') {
            delete assetStats.uniqueManufacturer[key];
          }
        }
        assetStats.uniqueMake = Object.keys(assetStats.uniqueManufacturer).length;
      }

      assetStats.totalAssets = assets.length;
      assetStats.pageCountTotal = assetStats.pageCountMono + assetStats.pageCountColor;
      assetStats.uniqueTonerCartridgeCount = assetStats.singleFunctionMono + assetStats.multiFunctionMono + 2 *
          (assetStats.singleFunctionColor + assetStats.multiFunctionColor);
      assetStats.uniqueModelCount = assetStats.models.length;

      return assetStats;
    }

    /**
     *  Get level stats
     * @param loadedLevel
     * @returns result
     */
    function getLevelStats(loadedLevel) {
      var result = {
        projectCount: 0,
        countryCount: 0,
        siteCount: 0,
        buildingCount: 0,
        floorCount: 0,
        zoneCount: 0
      };
      for (var index = 0; index < loadedLevel.length; index++) {
        switch (loadedLevel[index].type) {
          case LevelType.PROJECT:
            result.projectCount++;
            break;
          case LevelType.COUNTRY:
            result.countryCount++;
            break;
          case LevelType.SITE:
            result.siteCount++;
            break;
          case LevelType.BUILDING:
            result.buildingCount++;
            break;
          case LevelType.FLOOR:
            result.floorCount++;
            break;
        }
      }
      return result;
    }


    /**
     * get the Basic Stats
     * @param selectedLevel(Level will be there only when we need stats based on level else it will return stats of project)
     * @returns stats
     */
    function getBasicStats(selectedLevel) {
      var numberOfUsers, stats, subLevels,
          ratio = translateFilter('INFORMATION_NOT_AVAILABLE'),
          assets = AssetService.getRealAssets(),
          levels = LevelService.getLevels();
      assetStats = getAssetStats(assets, selectedLevel);
      if (selectedLevel) {
        subLevels = LevelService.getSubLevels(selectedLevel._id);
        subLevels.push(selectedLevel);
        levels = subLevels;
        for (var index = 0; index < levels.length; index++) {
          if (levels[index]._id.toString() === selectedLevel._id) {
            if (levels[index].totalEmployees) {
              numberOfUsers = levels[index].totalEmployees;
              var factor = Utils.getGcd(levels[index].totalEmployees, assetStats.plotted);
              ratio = levels[index].totalEmployees / factor + ":" + assetStats.plotted / factor;
            }
          }
        }
      }
      levelStats = getLevelStats(levels, selectedLevel);
      stats = {
        userToDeviceRatio: ratio,
        numberOfUsers: numberOfUsers,
        assetStats: assetStats,
        levelStats: levelStats
      };
      return stats;
    }

    return {
      getBasicStats: getBasicStats
    };
  }

  var app = angular.module('app'),
      requires = [
        'Configuration',
        'translateFilter',
        'ngCartosCore.constants.LevelType',
        'ngCartosCanvas.constants.AssetType',
        'StageHelperService',
        'ngCartosUtils.services.UtilityService',
        'ngCartosCore.services.AssetService',
        'ngCartosCore.services.LevelService',
        'ngCartosCore.services.LevelEntityMapService',
        StatsService
      ];
  app.factory('StatsService', requires);
}());