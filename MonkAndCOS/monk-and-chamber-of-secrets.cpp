#include<iostream>
#include<queue>
using namespace std;

queue<int>q;
int spidersInQueue,spidersToBeReplaced;


void readSpiderPower(int spidersInQueue, int spiderPower[])
{
    int position;
    for(position=0; position<spidersInQueue; position++)
    {
        cin>>spiderPower[position];
        q.push(position);
    }

}

int main()
{

    cin>>spidersInQueue>>spidersToBeReplaced;

    int spiderPower[spidersInQueue];
    readSpiderPower(spidersInQueue, spiderPower);
    for(int i=0; i<spidersToBeReplaced; i++)
    {
        int j;
        int ans[spidersToBeReplaced];
        for( j=0; j<spidersToBeReplaced; j++)
        {
            ans[j]=q.front();
            q.pop();
            if(q.empty())
            {
                j++;
                break;
            }
        }
        int max=ans[0];
        for(int k=1; k<j; k++)
        {
            if(spiderPower[max]<spiderPower[ans[k]])
                max=ans[k];
        }
        cout<<max+1<<" ";
        for(int k=0; k<j; k++)
        {
            if(ans[k]!=max)
            {
                if(spiderPower[ans[k]]>0)
                    spiderPower[ans[k]]--;
                q.push(ans[k]);
            }
        }
    }
}