#include <iostream>
#include <queue>
using namespace std;
void solve_monk_and_chamber_of_secret();
int deque_spiders(int dequeued_array[]);
int find_max_power(int j, int max, int dequeued_array[]);
int enqueue_spiders_after_decreasing_one_unit(int j, int max, int dequeued_array[]);
int total_number_of_spiders, number_of_selected_spiders;
int spider_power[] = {};
queue<int> power_queue;


void read_input()
{
    int position;
    cin >> total_number_of_spiders >> number_of_selected_spiders;
    for (position = 0; position < total_number_of_spiders; position++)
    {
        cin >> spider_power[position];
        power_queue.push(position);
    }
}

void solve_monk_and_chamber_of_secret()
{
    for (int iterator = 0; iterator < number_of_selected_spiders; iterator++)
    {
        int j = 0;
        int dequeued_array[number_of_selected_spiders];
        deque_spiders(dequeued_array);
        int max = dequeued_array[0];
        find_max_power(j, max, dequeued_array);
        enqueue_spiders_after_decreasing_one_unit(j, max, dequeued_array);
    }
}

int deque_spiders(int dequeued_array[])
{
    for (int j = 0; j < number_of_selected_spiders; j++)
    {
        dequeued_array[j] = power_queue.front();
        power_queue.pop();
        if (power_queue.empty())
        {
            j++;
            break;
        }
    }
}

int find_max_power(int j, int max, int dequeued_array[])
{
    for (int k = 1; k < j; k++)
    {
        if (spider_power[max] < spider_power[dequeued_array[k]])
        {
            max = dequeued_array[k];
        }
    }
    cout << max + 1 << " ";
}

int enqueue_spiders_after_decreasing_one_unit(int j, int max, int dequeued_array[])
{
    for (int k = 0; k < j; k++)
    {
        if (dequeued_array[k] != max)
        {
            if (spider_power[dequeued_array[k]] > 0)
            {
                spider_power[dequeued_array[k]]--;
            }
            power_queue.push(dequeued_array[k]);
        }
    }
}

int main()
{
    read_input();
    solve_monk_and_chamber_of_secret();
}