import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class RemoveFriend {
	public static void main(String args[]) throws Exception {
		solveRemoveFriends();
	}

	public static void solveRemoveFriends() throws Exception {
		public static BufferedReader br = new BufferedReader(new InputStreamReader(System. in ));
		String line = br.readLine();
		int numberOfTestCases = Integer.parseInt(line);
		printResult(numberOfTestCases);
	}

	public static void deleteFriend(numberOfTestCases) {

		while (numberOfTestCases-->0) {
			LinkedList < Integer > friendList = new LinkedList < Integer > ();
			String input[] = br.readLine().split(" ");
			int numberOfFriends = Integer.parseInt(input[0]);
			int noOfFriendsToDelete = Integer.parseInt(input[1]);
			String popularity[] = br.readLine().split(" ");
			friendList.push(Integer.parseInt(popularity[0]));
			int val = 0,
			friend = 0;

			checkPopularity(numberOfFriends, noOfFriendsToDelete, friendList);
			deleteLastFriend(friendList, popularity);
		}
	}

	public checkPopularity(int numberOfFriends, int noOfFriendsToDelete, List friendList) {
		for (friend = 1; friend < numberOfFriends && noOfFriendsToDelete > 0; friend++) {
			val = Integer.parseInt(popularity[friend]);
			while (!friendList.isEmpty() && noOfFriendsToDelete > 0) {
				if (val > friendList.peek()) {
					friendList.pop();
					noOfFriendsToDelete--;
				} else {
					break;
				}
			}

			friendList.push(val);
		}
	}

	public printResult(List friendList, String popularity[]) {
		while (!friendList.isEmpty()) {
			System.out.print(friendList.removeLast() + " ");
		}
		while (friend < numberOfFriends) {
			System.out.print(popularity[friend] + " ");
			friend++;
		}
		System.out.println();
	}
	
}