/*
 * FootBallFest.cpp
 *
 *  Created on: Feb 19, 2018
 *      Author: syed.fayaz
 */

#include "FootBallFest.h"
#include <stdio.h>

#define swap(a,b)({a = a + b; b = a - b; a = a - b;})

int find_final_possesser(int, char[], int[]);

int find_final_possesser(int no_of_passes, char action[], int possessor[]) {
	int previous_possesser, current_possesser;
	int pass_count, i = 1;

	previous_possesser = current_possesser = possessor[0];

	for (pass_count = 0; pass_count < no_of_passes; pass_count++) {

		if (action[pass_count] == 'P') {
			previous_possesser = current_possesser;
			current_possesser = possessor[i];
			i++;
		} else {
			swap(current_possesser, previous_possesser);
		}
	}

	return current_possesser;
}
