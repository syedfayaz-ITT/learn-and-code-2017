/*
 * FootBallFest_Tests.cpp
 *
 *  Created on: Feb 19, 2018
 *      Author: syed.fayaz
 */
#include<stdio.h>
#include "gtest/gtest.h"
#include "FootBallFest.h"

TEST(find_final_possesser, should_return_9) {
	int possesser[] = { 23, 86, 63, 60, 47, 99, 9 }, no_of_passes = 10;
	char action[] = { 'P', 'P', 'P', 'B', 'P', 'B', 'P', 'P', 'B', 'B' };
	int final_possesser = find_final_possesser(no_of_passes, action, possesser);
	ASSERT_EQ(9, final_possesser)<< "Should return player ID 9";
}

TEST(find_final_possesser, should_return_23) {
	int possesser[] = { 23 }, no_of_passes = 0;
	char action[] = { };
	int final_possesser = find_final_possesser(no_of_passes, action, possesser);
	ASSERT_EQ(23, final_possesser)<< "Should return player ID 23";
}

TEST(find_final_possesser, DISABLED_should_return_0) {
	int possesser[] = { }, no_of_passes = 0;
	char action[] = { };
	int final_possesser = find_final_possesser(no_of_passes, action, possesser);
	ASSERT_EQ(0, final_possesser)<< "Should return player ID 0";
}

GTEST_API_ int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

