#include <stdio.h>
#define PASS 'P'
#define BACK 'B'
int main()
{
    int currentPossessor, previousPossessor, noOfPasses, noOfTestCases, temp;
    char kindOfPass;

    scanf("%d", &noOfTestCases);

    while (noOfTestCases--)
    {
        scanf("%d %d", &noOfPasses, &currentPossessor);

        while (noOfPasses--)
        {
            scanf(" %c", &kindOfPass);

            if (kindOfPass == PASS)
            {
                previousPossessor = currentPossessor;
                scanf("%d", &currentPossessor);
            }
            else if (kindOfPass == BACK)
            {
                temp = previousPossessor;
                previousPossessor = currentPossessor;
                currentPossessor = temp;
            }
        }
        printf("Player %d\n", currentPossessor);
    }
    return 0;
}
