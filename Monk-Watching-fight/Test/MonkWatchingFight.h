/*
 * MonkWatchingFight.h
 *
 *  Created on: Feb 22, 2018
 *      Author: syed.fayaz
 */

int getMaxOf(int num1, int num2);
struct node* solveMonkWatchingFight(struct node *root, int numberOfElements,
		int element[]);
int height(struct node* node);
struct node* insert(struct node* node, int data);
struct node *createNode(int item);
