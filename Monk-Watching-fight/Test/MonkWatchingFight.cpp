/*
 * MonkWatchingFight.cpp
 *
 *  Created on: Feb 22, 2018
 *      Author: syed.fayaz
 */
#include <stdio.h>
#include<stdlib.h>
#include "MonkWatchingFight.h"

struct node {
	int data;
	struct node *left, *right;
};

int getMaxOf(int num1, int num2) {
	if (num1 >= num2) {
		return num1;
	} else {
		return num2;
	}
}

int height(struct node* node) {

	if (node == NULL) {
		return 0;
	}
	return 1 + getMaxOf(height(node->left), height(node->right));
}

struct node *createNode(int item) {
	struct node *newNode = (struct node *) malloc(sizeof(struct node));
	newNode->data = item;
	newNode->left = newNode->right = NULL;
	return newNode;
}

struct node* insert(struct node* node, int data) {

	if (node == NULL) {
		return createNode(data);
	}

	if (data <= node->data) {
		node->left = insert(node->left, data);
	} else if (data > node->data) {
		node->right = insert(node->right, data);
	}

	return node;
}

struct node* solveMonkWatchingFight(struct node *root, int numberOfElements,
		int element[]) {
	for (int i = 0; i < numberOfElements; i++) {
		root = insert(root, element[i]);
	}
	return root;
}

