public class AddAlternateElements {

	public static void addAlternateElements(int[] data) {
		final int TOTAL_NUMBER_OF_ELEMENTS = 9;
		int oddSum = 0, evenSum = 0;

		Utility utils = new Utility();

		for (int i = 0; i < TOTAL_NUMBER_OF_ELEMENTS; i++) {
			if (utils.modulusOf(i) == 0) {
				oddSum = utils.add(oddSum, data[i]);
			} else {
				evenSum = utils.add(evenSum, data[i]);
			}
		}
		System.out.println(oddSum);
		System.out.println(evenSum);
	}

	public static void main(String args[]) throws Exception {
		int[] data = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		addAlternateElements(data);
	}
}
