const int TABLE_SIZE = 128;

class HashEntry
{
public:
    int key;
    int value;
    HashEntry(int key, int value)
    {
        this->key = key;
        this->value = value;
    }
};

class HashMap
{
private:
    HashEntry **table;
public:
    HashMap()
    {
        table = new HashEntry * [TABLE_SIZE];
        for (int i = 0; i< TABLE_SIZE; i++)
        {
            table[i] = NULL;
        }
    }

    int HashFunc(int key)
    {
        return key % TABLE_SIZE;
    }
    
    void Insert(int key, int value)
    {
        int hash = HashFunc(key);
        while (table[hash] != NULL && table[hash]->key != key)
        {
            hash = HashFunc(hash + 1);
        }
        if (table[hash] != NULL)
            delete table[hash];
        table[hash] = new HashEntry(key, value);
    }
   
    int Search(int key)
    {
        int  hash = HashFunc(key);
        while (table[hash] != NULL && table[hash]->key != key)
        {
            hash = HashFunc(hash + 1);
        }
        if (table[hash] == NULL)
            return -1;
        else
            return table[hash]->value;
    }

    ~HashMap()
    {
        for (int i = 0; i < TABLE_SIZE; i++)
        {
            if (table[i] != NULL)
                delete table[i];
            delete[] table;
        }
    }
};
